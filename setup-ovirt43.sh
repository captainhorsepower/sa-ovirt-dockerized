echo 'Building required docker images:'

echo '
Base postgres:10 image
'
docker build -t pgovirt:10 -f postgres/Dockerfile postgres/

echo '
Datawarehouse database
'
docker build -t dwhdb -f postgres/dwhdb/dwhdb.dockerfile postgres/dwhdb/

echo '
Engine database
'
docker build -t ngndb -f postgres/ngndb/enginedb.dockerfile postgres/ngndb/

echo '
Centos7 with deps for ovirt4.3 and postgres 10.12'
echo 'This will take a while...
'
docker build -t ngn43-rpm -f ngn-dwh/ovirt43-rpm.dockerfile ngn-dwh/

echo '
Centos7 running engine and datawarehouse
'
docker build -t sa-ngn-dwh -f ngn-dwh/sa-engine-dwh.dockerfile ngn-dwh/

echo '
Starting containers...
'
docker-compose up -d

echo '
Waiting a bit until they load...'
sleep 5;

echo '
Enjoy setup process!
'
container=$(docker ps -f "name=.*engine-dwh-43.*" -q)
docker exec -it $container bash -c 'engine-setup --config=setup.ans'
