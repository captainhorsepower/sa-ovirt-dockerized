FROM pgovirt:10
MAINTAINER "Artsiom Varabei" <av@aircloud.org>

COPY enginedb.sh /docker-entrypoint-initdb.d/
RUN chmod a+r /docker-entrypoint-initdb.d/enginedb.sh


