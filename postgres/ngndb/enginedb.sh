#!/bin/bash

psql -d template1 -c "create user engine password 'aircloud';" && \
psql -d template1 -c "create database engine owner engine template template0 encoding 'UTF8' lc_collate 'en_US.UTF-8' lc_ctype 'en_US.UTF-8';"

psql -d engine -c 'DROP FUNCTION IF EXISTS uuid_generate_v1();'
psql -d engine -c 'CREATE EXTENSION "uuid-ossp";'
