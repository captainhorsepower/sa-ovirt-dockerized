#!/bin/bash

psql -d template1 -c "create user ovirt_engine_history password 'aircloud';" && \
psql -d template1 -c "create database ovirt_engine_history owner ovirt_engine_history template template0 encoding 'UTF8' lc_collate 'en_US.UTF-8' lc_ctype 'en_US.UTF-8';"

psql -d ovirt_engine_history -c 'DROP FUNCTION IF EXISTS uuid_generate_v1();'
psql -d ovirt_engine_history -c 'CREATE EXTENSION "uuid-ossp";'
