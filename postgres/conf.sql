ALTER SYSTEM SET autovacuum_vacuum_scale_factor = 0.01;
ALTER SYSTEM SET autovacuum_analyze_scale_factor = 0.075;
ALTER SYSTEM SET autovacuum_max_workers = 6;
ALTER SYSTEM SET maintenance_work_mem = 65536;
ALTER SYSTEM SET max_connections = 150;
ALTER SYSTEM SET lc_messages = 'en_US.UTF-8';
ALTER SYSTEM SET work_mem = '8MB';
